provider "aws" {
    region = "us-east-1b"
}

resource "aws_instance" "webserver" {
    count = 1
    instance_type = "t3a.2xlarge"
}

output "public_ip" {
    value = aws_instance.webserver.public_ip
}

